#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <event.h>
#include <pthread.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <parted/device.h>
#include <parted/disk.h>

#include "nfsd4_block.h"
#include "efi.h"

typedef struct token {
	char	*buf,
	seperator,
	*ptr;
	int	len;
} token_t;

typedef bl_comm_res_t *(*ctl_func_t)(bl_comm_msg_t *, bl_comm_res_t *, int *);

typedef struct ctl_table {
	int		msg_type;	// Used for validation
	char		*msg_name;	// Used for debug
	ctl_func_t	msg_func;
} ctl_table_t;

void process_msg(int fd, short ev_type, void *arg);
void *upcall_vers_check(void *);
bl_comm_res_t *do_stop(bl_comm_msg_t *msg, bl_comm_res_t *res, int *);
bl_comm_res_t *get_sig(bl_comm_msg_t *msg, bl_comm_res_t *res, int *);
bl_comm_res_t *get_slice(bl_comm_msg_t *msg, bl_comm_res_t *res, int *);
bl_comm_res_t *check_dm(bl_comm_msg_t *msg, bl_comm_res_t *res, int *);
bl_comm_res_t *get_dm(bl_comm_msg_t *msg, bl_comm_res_t *res, int *);
bl_comm_res_t *get_vers(bl_comm_msg_t *msg, bl_comm_res_t *res, int *);


/* ---- Utility prototypes ---- */
int set_operation(int op);
void Usage(char *);
void daemonize();
static token_t * __token_init(const char *input, char seperator);
static void __token_fini(token_t *t);
static int __token_count(token_t *t);
static const char * __token_next(token_t *t);
static const char * __token_str(token_t *t);
static int __token_next_int(token_t *t);
static int get_output(char *prog, const char **argv);
static char *find_dm_name(int major, int minor);
static void hexdump(char *p, int len, int off);

/*
 * NOTE: This table must be in the same order as the #defines are listed
 * in nfsd4_block.h else there will be trouble.
 */
ctl_table_t table[] = {
	{ PNFS_UPCALL_MSG_STOP,		"MSG_STOP    ",	do_stop },
	{ PNFS_UPCALL_MSG_GETSIG,	"MSG_GETSIG  ",	get_sig },
	{ PNFS_UPCALL_MSG_GETSLICE,	"MSG_GETSLICE",	get_slice },
	{ PNFS_UPCALL_MSG_DMCHK,	"MSG_DMCHK   ",	check_dm },
	{ PNFS_UPCALL_MSG_DMGET,	"MSG_DMGET   ",	get_dm },
	{ PNFS_UPCALL_MSG_VERS,		"MSG_VERS    ",	get_vers },
};


struct event	ge;

int
main(int argc, char **argv)
{
	int		upcall_fd,
			operation	= -1,
			len;
	char		c,
			*prog;
	pthread_t	t;
	void		*thr_val;

	if ((prog = strrchr(*argv, '/')) == NULL)
		prog = *argv;
	else
		prog++;

	while ((c = getopt(argc, argv, "du")) != EOF)
		switch (c) {
		case 'd':
			operation = PNFS_BLOCK_CTL_STOP;
			break;

		case 'u':
			operation = PNFS_BLOCK_CTL_START;
			break;
				
		default:
			Usage(prog);
			exit(1);
		}

	if (operation == -1) {
		Usage(prog);
		exit(1);
	}

	if (set_operation(operation))
		exit(1);

	if (operation == PNFS_BLOCK_CTL_START) {
		daemonize();
		event_init();
		if ((upcall_fd = open("/var/lib/nfs/rpc_pipefs/nfs/pnfs_block",
		    O_RDWR, 0)) == -1) {
			perror("/var/lib/nfs/rpc_pipefs/nfs/pnfs_block");
			exit(1);
		}
		event_set(&ge, upcall_fd, EV_READ, process_msg, &ge);
		event_add(&ge, NULL);

		if (pthread_create(&t, NULL, upcall_vers_check, NULL)) {
			fprintf(stderr, "%s: failed to create thread\n", prog);
			exit(1);
		}

		event_dispatch();
		printf("daemon finished processing data\n");
		close(upcall_fd);
		pthread_join(t, &thr_val);
	}
}

/*
 * upcall_vers_check -- Validate that the kernel and daemon are in synch.
 *
 * Sending PNFS_BLOCK_CTL_KICK to the kernel will cause the kernel in turn
 * to send a test message back to the daemon.
 */
void *
upcall_vers_check(void *v)
{
	if (set_operation(PNFS_BLOCK_CTL_VERS))
		exit(1);
	pthread_exit(NULL);
	return NULL;
}

void
process_msg(int fd, short ev_type, void *arg)
{
	bl_comm_msg_t	msg;
	struct event	*ev = (struct event *)arg;
	bl_comm_res_t	*resp;
	int		len, cc;

	resp = calloc(1, sizeof (*resp));
	if (!resp)
		return;
	len = sizeof (bl_comm_res_t);
	if ((cc = read(fd, &msg, sizeof (msg))) != sizeof (msg)) {
		printf("Failed to get all of message. Got %d, expected %d\n",
		    cc, sizeof (msg));
		resp->res_status = PNFS_BLOCK_FAILURE;
		write(fd, resp, len);
		return;
	}
	if (msg.msg_type > PNFS_UPCALL_MSG_VERS) {
		printf("msg_type(%d) value to large. Expecting nothing higher than %d\n",
		       msg.msg_type, PNFS_UPCALL_MSG_VERS);
		resp->res_status = PNFS_BLOCK_FAILURE;
	} if (msg.msg_type != table[msg.msg_type].msg_type) {
		printf("type mismatch. Request of %d has msg_type of %d\n",
		       msg.msg_type, table[msg.msg_type].msg_type);
		resp->res_status = PNFS_BLOCK_FAILURE;
	} else {
		printf("Processing: %s: ", table[msg.msg_type].msg_name);
		resp = table[msg.msg_type].msg_func(&msg, resp, &len);
	}

	/* ---- do_stop() sets the len to negative ---- */
	if (len < 0) {
		write(fd, resp, -len);
		exit(0);
	} else
		write(fd, resp, len);
	free(resp);
	event_add(ev, NULL);
}

bl_comm_res_t *
do_stop(bl_comm_msg_t *msg, bl_comm_res_t *res, int *len)
{
	printf("Got request to stop\n");
	*len = -(*len);
	res->res_status = PNFS_BLOCK_SUCCESS;
	return res;
}

bl_comm_res_t *
get_sig(bl_comm_msg_t *msg, bl_comm_res_t *res, int *lenp)
{
	int	fd	= -1,
		major,
		minor,
		dev,
		len;
	char	*path	= "/tmp/sda";
	struct partition_sig	sig;
	bl_comm_res_t		*new_res;
	
	sig.ps_sig = NULL;
	major = (msg->u.msg_dev >> 20) & 0xfff;
	minor = msg->u.msg_dev & 0xfffff;
	dev = (major << 8) | (minor & ~0xf);

	res->res_status = PNFS_BLOCK_FAILURE;	// Default condition
	
	if (mknod(path, S_IFBLK | 0600, dev)) {
		perror("mknod");
		goto out;
	}
	fd = open(path, O_RDONLY);
	(void) unlink(path);
	if (fd == -1) {
		perror(path);
		goto out;
	}
	
	if (!efi_signature(fd, &sig)) {
		fprintf(stderr, "Failed to get EFI signature\n");
		goto out;
	}
	printf("%s: Got sig at: %llu, within sector %d, len %d\n", __func__,
	       sig.ps_sector, sig.ps_sector_off, sig.ps_len);
	len = sizeof (*new_res) + sig.ps_len;
	new_res = calloc(1, len);
	if (!new_res)
		goto out;
	*lenp = len;
	free(res);
	res			= new_res;
	res->res_status		= PNFS_BLOCK_SUCCESS;
	res->u.sig.sector	= sig.ps_sector;
	res->u.sig.offset	= sig.ps_sector_off;
	res->u.sig.len		= sig.ps_len;
	memcpy(res->u.sig.sig, sig.ps_sig, sig.ps_len);
	hexdump(res->u.sig.sig, sig.ps_len, 4);
	
out:
	if (sig.ps_sig)
		free(sig.ps_sig);
	if (fd != -1)
		close(fd);
	return res;
}

bl_comm_res_t *
get_vers(bl_comm_msg_t *msg, bl_comm_res_t *res, int *len)
{
	if (msg->u.msg_vers != PNFS_UPCALL_VERS) {
		fprintf(stderr,
			"Version mismatch between kernel and daemon\n");
		exit(1);
	}
	printf("Version: %d\n", PNFS_UPCALL_VERS);
	res->res_status = PNFS_BLOCK_SUCCESS;
	res->u.vers = PNFS_UPCALL_VERS;
	return res;
}

bl_comm_res_t *
get_dm(bl_comm_msg_t *msg, bl_comm_res_t *res, int *lenp)
{
	const char	*argv[4] = {"dmsetup", "table", NULL, NULL};
	char	buf[128],
		*name;
	int	major,
		minor,
		len,
		fd,
		i,
		num_of_stripes,
		stripe_size;
	FILE	*fp;
	token_t	*t;
	
	major = (msg->u.msg_dev >> 20) & 0xfff;
	minor = msg->u.msg_dev & 0xfffff;
	name = find_dm_name(major, minor);
	if (!name) {
		res->res_status = PNFS_BLOCK_FAILURE;
		return res;
	}
	argv[2] = name;
	if (((fd = get_output("/sbin/dmsetup", argv)) == -1) ||
	    ((fp = fdopen(fd, "r")) == NULL)) {
		printf("%s: get_output/fdopen failed\n", __func__);
		res->res_status = PNFS_BLOCK_FAILURE;
		return res;
	}
	free(name);
	if (fgets(buf, sizeof (buf), fp) == NULL) {
		printf("%s: fgets failed\n", __func__);
		res->res_status = PNFS_BLOCK_FAILURE;
		return res;
	}
	printf("Vol info: %s", buf);
	t = __token_init(buf, ' ');
	(void) __token_next(t);	// toss start offset for DM volume
	(void) __token_next(t); // toss size of DM volume
	if (strcmp("striped", __token_next(t))) {
		printf("%s: DM volume isn't striped\n", __func__);
		res->res_status = PNFS_BLOCK_FAILURE;
		return res;
	}
	
	num_of_stripes = __token_next_int(t);
	stripe_size = __token_next_int(t);
	
	free(res);
	len = sizeof (bl_comm_res_t) + (num_of_stripes * sizeof (stripe_dev_t));
	res = calloc(1, len);
	if (!res)
		return NULL;	// We're hosed!
	res->res_status = PNFS_BLOCK_SUCCESS;
	*lenp = len;
	res->u.stripe.num_stripes = num_of_stripes;
	res->u.stripe.stripe_size = stripe_size;
	
	for (i = 0; i < num_of_stripes; i++) {
		token_t	*dev_token;
		
		dev_token = __token_init(__token_next(t), ':');
		res->u.stripe.devs[i].offset = __token_next_int(t);
		res->u.stripe.devs[i].major = __token_next_int(dev_token);
		res->u.stripe.devs[i].minor = __token_next_int(dev_token);
		printf("    major %d, minor %d\n",
		    res->u.stripe.devs[i].major, res->u.stripe.devs[i].minor);
		__token_fini(dev_token);
	}
	__token_fini(t);
	return res;
}

bl_comm_res_t *
check_dm(bl_comm_msg_t *msg, bl_comm_res_t *res, int *len)
{
	int	fd,
		major,
		minor;
	char	*name;
	
	res->res_status = PNFS_BLOCK_SUCCESS;
	res->u.dm_vol = 0;

	major = (msg->u.msg_dev >> 20) & 0xfff;
	minor = msg->u.msg_dev & 0xfffff;
	name = find_dm_name(major, minor);
	res->u.dm_vol = name ? 1 : 0;
	printf("DM Volume: %s\n", res->u.dm_vol ? "True" : "False");
	free(name);
	
	return res;
}

bl_comm_res_t *
get_slice(bl_comm_msg_t *msg, bl_comm_res_t *res, int *len)
{
	/*
	 * Grrr.. The parted code complains if anything other than sda is used as a name.
	 */
	char		*path		= "/tmp/sda";
	dev_t		dev,
			minor,
			major;
	int		partition	= msg->u.msg_dev & 0xfffff;
	PedDevice	*pdev;
	PedDisk		*pdisk;
	PedPartition	*ppart;
	
	major = (msg->u.msg_dev >> 20) & 0xfff;
	minor = msg->u.msg_dev & 0xfffff;
	dev = (major << 8) | (minor & ~0xf);
	partition = minor & 0xf;
	res->res_status = PNFS_BLOCK_FAILURE;	// Default condition
	
	if (mknod(path, S_IFBLK | 0600, dev)) {
		perror("mknod");
		goto out;
	}
	
	pdev = ped_device_get(path);
	if (pdev == NULL) {
		fprintf(stderr, "ped_device_get(%s) failed\n", path);
		goto out;
	}
	(void)ped_device_open(pdev);	// Is this required for further access?
	
	pdisk = ped_disk_new(pdev);
	if (pdisk == NULL) {
		fprintf(stderr, "ped_disk_new() failed\n");
		goto out;
	}
	
	ppart = ped_disk_get_partition(pdisk, partition);
	if (ppart == NULL) {
		fprintf(stderr, "ped_disk_get_partiton(%p, %d) failed\n",
			pdisk, partition);
		goto out;
	}
	
	res->res_status = PNFS_BLOCK_SUCCESS;
	res->u.slice.start = ppart->geom.start;
	res->u.slice.length = ppart->geom.length;
	printf("Dev: %d:%d, Start: %llu, Length: %llu\n", major, minor,
	       res->u.slice.start, res->u.slice.length);
out:
	(void) unlink(path);

	return res;
}

/*
 * []---------------------------------------------------------------------[]
 * | Utility functions						|
 * []---------------------------------------------------------------------[]
 */

void
Usage(char *prog)
{
	fprintf(stderr, "Usage: %s -u|-d\n", prog);
}

/*
 * For now leave all of the file descriptors open in the child to allow debug messages
 * to be printed on the console.
 */
void
daemonize()
{
	switch (fork()) {
		case 0:
			break;
		case -1:
			perror("fork");
			exit(1);
		default:
			exit(0);
	}
}

int
set_operation(int op)
{
	int	ctl_fd,
	operation = op;
	
	if ((ctl_fd = open("/proc/fs/pnfs_block/ctl", O_RDWR, 0)) == -1) {
		perror("/proc/fs/pnfs_block/ctl");
		return 1;
	}
	if (write(ctl_fd, &operation, sizeof (operation)) !=
	    sizeof (operation)) {
		perror("write");
		return 1;
	}
	close(ctl_fd);
	return 0;
}

static char *
find_dm_name(int major, int minor)
{
	const char	*argv[5] = {"dmsetup", "ls", "--target", "striped", NULL};
	char		buf[128],
			*name;
	const char	*majmin;
	int		fd,
			status,
			maj,
			min;
	FILE		*fp;
	token_t		*t	= NULL,
			*t1	= NULL;
	
	if (((fd = get_output("/sbin/dmsetup", argv)) == -1) ||
	    ((fp = fdopen(fd, "r")) == NULL)) {
		printf("%s: get_output/fdopen failed\n", __func__);
		return;
	}
	while (fgets(buf, sizeof (buf), fp) != NULL) {
		t = __token_init(buf, '\t');
		if (__token_count(t) != 2) {
			printf("Parsing error for %s\n", __token_str(t));
			goto error;
		}
		/*
		 * Need to duplicate the string name here because later on
		 * the token object is freed and the reference gets erased.
		 */
		name = strdup(__token_next(t));

		majmin = __token_next(t);
		if ((*majmin != '(') || (strchr(majmin, ')') == NULL)) {
			printf("Invalid major/minor number string: %s\n", majmin);
			goto error;
		}
		
		t1 = __token_init(majmin + 1, ',');
		if (__token_count(t1) != 2) {
			printf("Invalid count for major/minor pair: %s", __token_str(t1));
			goto error;
		}
		maj = __token_next_int(t1);
		min = __token_next_int(t1);
		__token_fini(t);	t = NULL;
		__token_fini(t1);	t1 = NULL;
		if ((major == maj) && (minor == min))
			break;
		else {
			free(name);
			name = NULL;
			printf("major(%d) != maj(%d) or minor(%d) = min(%d)\n",
			       major, maj, minor, min);
		}
	}
	
	fclose(fp);
	(void) wait(&status);
	return name;
	
error:
	if (t)
		__token_fini(t);
	if (t1)
		__token_fini(t1);
	
	fclose(fp);
	(void) wait(&status);
}

static int
get_output(char *prog, const char **argv)
{
	int	fildes[2];
	
	if (pipe(fildes) == -1) {
		perror("pipe");
		return -1;
	}
	
	switch (fork()) {
		case -1:
			perror("fork");
			return -1;
			
		case 0:
			close(fildes[0]);
			dup2(fildes[1], 1);
			dup2(fildes[1], 2);
			if (execv(prog, argv) == -1) {
				perror("execv");
				exit(1);
			}
			break;
			
			default:
			close(fildes[1]);
			return fildes[0];
	}
}

static token_t *
__token_init(const char *input, char seperator)
{
	token_t	*t;
	
	t = malloc(sizeof (*t));
	if (!t)
		return NULL;
	
	t->seperator = seperator;
	t->len = strlen(input) + 1;
	t->buf = malloc(t->len);
	if (!t->buf) {
		free(t);
		return NULL;
	}
	strncpy(t->buf, input, t->len);
	t->ptr = t->buf;
	return t;
}

static void
__token_fini(token_t *t)
{
	free(t->buf);
	free(t);
}

#ifdef not_yet_needed
static void
__token_reset(token_t *t)
{
	int	count	= t->len - 1;	// don't touch the final null
	char	*p;
	
	t->ptr = t->buf;
	for (p = t->ptr; count; count--, p++)
		if (*p == '\0')
			*p = t->seperator;
}
#endif

static int
__token_count(token_t *t)
{
	int	count	= 1;
	char	*p	= t->ptr;
	
	if (*t->ptr == '\0')
		return 0;
	while (*p) {
		p = strchr(p, t->seperator);
		if (!p)
			return count;
		else
			p++;
		count++;
	}
	return count;
}

static const char *
__token_next(token_t *t)
{
	char	*p = t->ptr;
	
	if (p) {
		t->ptr = strchr(t->ptr, t->seperator);
		if (t->ptr)
			*t->ptr++ = '\0';
	}
	return p;
}

static int
__token_next_int(token_t *t)
{
	return strtol(__token_next(t), NULL, 0);
}

static const char *
__token_str(token_t *t)
{
	return (t->ptr);
}

static void
hexdump(char *p, int len, int off)
{
	int	i;
	
	if (off)
		printf("%*s", off, " ");
	for (i = 1; i <= len; p++, i++) {
		printf("%02x ", *p & 0xff);
		if (!(i % 8))
			printf("  ");
		if (!(i % 16)) {
			printf("\n");
			if (i != len)
				printf("%*s", off, " ");
		}
	}
}
