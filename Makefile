OBJS=	ctl.o efi.o
CFLAGS= -I../../linux-pnfs.working/include

ctl: $(OBJS)
	cc -o ctl $(OBJS) -levent -lpthread -lparted

clean:
	rm -f $(OBJS) ctl
